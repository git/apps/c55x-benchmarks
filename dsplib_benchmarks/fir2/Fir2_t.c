/*
 * Copyright (C) 2016 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

//*****************************************************************************
//  Filename:	 fir2_t.c
//  Version:	 0.01
//  Description: test for fir2 routine
//  Revision History:	 
//    2  R. Katzur   03/30/16   Benchmark
//*****************************************************************************

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <tms320.h>
#include <dsplib.h>
#include <time.h>

#define  NUMBER_OF_ITERATIONS   1000l


//#include "t1.h"
//#include "t2.h"
//#include "t3.h"
//#include "t4.h"
//#include "t5.h"
//#include "t6.h"
//#include "t7.h"
#include "t5_ran.h"
//#include "test.h"

short test(DATA *r, DATA *rtest, short n, DATA maxerror);

short eflag1= PASS;
short eflag2= PASS;

#pragma DATA_SECTION(r, ".bss:saram")
DATA  r[NX];

DATA  *dbptr = &db[0];
void clear_ran(short x[], int N)
{
	int i   ;
	for (i=0; i<N; i++)
	{
		x[i] = 0   ;
	}
	return ;
}

void main()
{
    short i;
	   int nx  ;
	   long  iterations1   ;
	   clock_t   t1,t2, t11,t22 ,total1_t ,total2_t,diff  ;


    // 1. Test for single-buffer

    // clear

    clear_ran(db, NH+2) ;

    // compute

    nx = 32;
    t1 = clock()   ;
    t2 = clock()  ;
    diff = t2 - t1 ;   ///   overhead of calling
    t1 = clock()   ;   // compute


    (void)fir2(x, h, r, dbptr, nx, NH);



    t2 = clock()  ;

    total1_t = (double) (t2 - t1-diff)   ;
    printf("256 tap, %d values FIR Real 16-bit   \n" , nx) ;
    printf("fir2 time (in cycles)  %ld \n", total1_t) ;


   

    // test
    eflag1 = test(r, rtest, nx, MAXERROR);

  
    if( (eflag1 != PASS) || (eflag2 != PASS) )
    {
        exit(-1);
    }
  
  
  
    for (iterations1 = 0; iterations1 < NUMBER_OF_ITERATIONS;iterations1++)

    {

        (void)fir2(x, h, r, dbptr, 4, NH);

    }


    printf("Done with %ld iteration  \n",iterations1 );





    return;
}
