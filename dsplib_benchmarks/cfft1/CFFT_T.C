/*
 * Copyright (C) 2016 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
//  Revision History:	 
//    2  R. Katzur   03/30/16   Benchmark


#include <stdlib.h>
#include <math.h>
#include "tms320.h"
#include <stdio.h>
#include <dsplib.h>
#include <time.h>

#define  NUMBER_OF_ITERATIONS   1000000l
#define FFT_HARDWARE   1
//#include "t1_SCALE.h"
//#include "t2_SCALE.h" //16
//#include "t3_SCALE.h" //32
#include "t4_SCALE.h" //64
//#include "t5_SCALE.h" //128
//#include "t6_SCALE.h" //256
//#include "t7_SCALE.h" //512
//#include "t8_SCALE.h" //1024
//#include "t2_NOSCALE.h"
//#include "t3_NOSCALE.h"
//#include "t4_NOSCALE.h"
//#include "t5_NOSCALE.h"
//#include "t6_NOSCALE.h"
//#include "t7_NOSCALE.h"
//#include "t8_NOSCALE.h"


//#include "hwafft.h"

#ifndef SCALING
#define SCALING 0
#endif

short test(DATA *r, DATA *rtest, short n, DATA maxerror);

short eflag = PASS;

////////////////////////////////////////////////
long  *data;
long  *data_br;
unsigned short  fft_flag = 0 ;  //0 ->fft  1 ->ifft
unsigned short scale_flag= 0; //0 -> scaling   1-> no scalling
DATA *result ;
unsigned short   sel_out;

       short compare_flag  ;
       int i   ;
	   int nx  ;
	   long  iterations1   ;
	   clock_t   t1,t2, t11,t22 ,total1_t ,total2_t,diff , total3_t ;
	   long  (*fp)  (DATA , DATA , unsigned short , unsigned short );


	   
	   
	   
	   
#if FFT_HARDWARE    //  when it is 1, we use the hardware FFT

void main()
{

     	short xx,yy  ;
        short maxError   ;
        nx = NX   ;

	    t1 = clock()   ;
	    t2 = clock()  ;
	    diff = t2 - t1 ;   ///   overhead of calling
		
		
		 *(volatile ioport unsigned short *)0x0001 = 0x000E;
			asm("   idle");
			asm ("   nop  ")  ;
			asm ("   nop  ")  ;
			asm ("   nop  ")  ;

    t1 = clock()   ;
	    hwafft_br(x, x_br, nx) ;

    t2 = clock()  ;
     total1_t = (double) (t2 - t1-diff)   ;
	     printf(" Using bit reversal Number of elements  is %d  \n",nx ) ;
	     printf("Bit reversal accelerator  time (in cycles)  %ld \n", total1_t) ;
	     
//	     for (i=0; i<2*nx; i++)
//	         x_br[i] = x[i]   ;
#if SCALING
	     scale_flag = 0;
#else
	         scale_flag = 1;
#endif


	     if (nx == 8)
	     {
	         t1 = clock()   ;

	         sel_out =hwafft_8pts(x_br, scratch, fft_flag, scale_flag );

            t2 = clock()  ;
	     }

	     if (nx == 16)
	     {
	         t1 = clock()   ;

	         sel_out =hwafft_16pts(x_br, scratch, fft_flag, scale_flag );

            t2 = clock()  ;
	     }

	     if (nx == 32)
	     {
	         t1 = clock()   ;

	         sel_out =hwafft_32pts(x_br, scratch, fft_flag, scale_flag );

            t2 = clock()  ;
	     }
	     if (nx == 64)
	     {
	         t1 = clock()   ;

	         sel_out =hwafft_64pts(x_br, scratch, fft_flag, scale_flag );

            t2 = clock()  ;
	     }
	     if (nx == 128)
	     {
	         t1 = clock()   ;

	         sel_out =hwafft_128pts(x_br, scratch, fft_flag, scale_flag );

            t2 = clock()  ;
	     }
	     if (nx == 256)
	     {
	         t1 = clock()   ;

	         sel_out =hwafft_256pts(x_br, scratch, fft_flag, scale_flag );

            t2 = clock()  ;
	     }
	     if (nx == 512)
	     {
	         t1 = clock()   ;

	         sel_out =hwafft_512pts(x_br, scratch, fft_flag, scale_flag );

            t2 = clock()  ;
	     }
	     if (nx == 1024)
	     {
	         t1 = clock()   ;

	         sel_out =hwafft_1024pts(x_br, scratch, fft_flag, scale_flag );

            t2 = clock()  ;
	     }









	     if (sel_out == 0)
	        {
	            result = x_br;
	        }
	     else

		   {
		              result = scratch;
		   }


           total1_t = (double) (t2 - t1-diff)   ;
    printf(" Complex FFT number of elements is %d   \n", nx) ;
    printf("fft time (in cycles)  %ld \n", total1_t) ;

    maxError = 0 ;
    for (i=0; i<2*nx; i++)
    {

    	xx = result[i] - rtest[i] ;
    	yy = abs(xx) ;
    	if (yy > maxError)  maxError = yy  ;
    }


	   printf(" max Error =   %d  \n", maxError)    ;
	
	
   // test
    eflag = test(result, rtest, NX, MAXERROR);

    if(eflag != PASS)
    {
        exit(-1);
    }
	
	
	

    printf(" number of errors  %d  \n", compare_flag)    ;
    if (nx == 8)
    {
        for (iterations1 = 0; iterations1 < NUMBER_OF_ITERATIONS;iterations1++)

       {
            sel_out =hwafft_8pts(x_br, scratch, fft_flag, scale_flag );
       }

    }


  if (nx == 16)
    {
        for (iterations1 = 0; iterations1 < NUMBER_OF_ITERATIONS;iterations1++)

       {
            sel_out =hwafft_16pts(x_br, scratch, fft_flag, scale_flag );
       }
    }



    if (nx == 32)
    {
        for (iterations1 = 0; iterations1 < NUMBER_OF_ITERATIONS;iterations1++)

       {
            sel_out =hwafft_32pts(x_br, scratch, fft_flag, scale_flag );
       }
    }



    if (nx == 64)
    {
        for (iterations1 = 0; iterations1 < NUMBER_OF_ITERATIONS;iterations1++)

       {
            sel_out =hwafft_64pts(x_br, scratch, fft_flag, scale_flag );
       }
    }



    if (nx == 128)
    {
        for (iterations1 = 0; iterations1 < NUMBER_OF_ITERATIONS;iterations1++)

       {
            sel_out =hwafft_128pts(x_br, scratch, fft_flag, scale_flag );
       }
    }



    if (nx == 256)
    {
        for (iterations1 = 0; iterations1 < NUMBER_OF_ITERATIONS;iterations1++)

       {
            sel_out =hwafft_256pts(x_br, scratch, fft_flag, scale_flag );
       }
    }



    if (nx == 512)
    {
        for (iterations1 = 0; iterations1 < NUMBER_OF_ITERATIONS;iterations1++)

       {
            sel_out =hwafft_512pts(x_br, scratch, fft_flag, scale_flag );
       }
    }


;
    if (nx == 1024)
    {
        for (iterations1 = 0; iterations1 < NUMBER_OF_ITERATIONS;iterations1++)

       {
            sel_out =hwafft_1024pts(x_br, scratch, fft_flag, scale_flag );
       }
    }





    printf("Done with %ld iteration nx = %d \n",iterations1, nx );

   

    return;
}


		      




#else

void main()
{


	   int nx  ;
	   long  iterations1   ;
	   clock_t   t1,t2, t11,t22 ,total1_t ,total2_t,diff  ;

	    t1 = clock()   ;
	    t2 = clock()  ;
	    diff = t2 - t1 ;   ///   overhead of calling
	    t1 = clock()   ;
    // compute
#if SCALING    
    cfft(x, NX, SCALE);
#else    
    cfft(x, NX, NOSCALE);
#endif
    nx = NX  ;
    t2 = clock()  ;
    total1_t = (double) (t2 - t1-diff)   ;
    printf(" Complex FFT number of elements is %d  \n",nx ) ;
    printf("fft time (in cycles)  %ld \n", total1_t) ;
    t11 = clock()   ;

    cbrev(x, x, NX);

    t22 = clock()  ;
    total2_t = (double) (t22 - t11- diff)   ;

    printf("bit reverse time (in cycles)  %ld \n", total2_t) ;    ;
    
    // test
    eflag = test(x, rtest, NX, MAXERROR);

    if(eflag != PASS)
    {
        exit(-1);
    }



    for (iterations1 = 0; iterations1 < NUMBER_OF_ITERATIONS;iterations1++)

    {

#if SCALING
    cfft(x, NX, SCALE);
#else
    cfft(x, NX, NOSCALE);
#endif

    }


    printf("Done with %ld iteration  \n",iterations1 );



    return;
}


#endif
